fyne(1) -- Command line utilty to package Fyne applications for various OSes.
=============================================

## SYNOPSIS

`fyne` [commmand] [parameters]

## DESCRIPTION

Just specifying the target OS and any required metadata (such as icon)
will generate the appropriate package. The icon conversion will be done
automatically for .icns or .ico so just provide a .png file.
All you need is to have the application already built for the target platform.

## COMMANDS

`bundle` [parameters] *file|directory*

    The bundle command embeds static content into your go application.
    Each resource will have a generated filename unless specified

`package` [parameters]
    
    The package command prepares an application for distribution.
    Before running this command you must to build the application for release.

`help` [package|bundle]
  
    Print help messages.

## BUNDLE PARAMETERS

 * `-append` :
  Append an existing go file (don't output headers)

 * `-name` *string* :
  The variable name to assign the resource (file mode only)

 * `-package` *string* :
  The package to output in headers (if not appending) (default "main")

 * `-prefix` *string* :
  A prefix for variables (ignored if name is set) (default "resource")

## PACKAGE PARAMETERS

 * `-executable` *string* :
  The path to the executable, default is the current dir main binary
 
 * `-icon` *string* :
  The name of the application icon file

 * `-name` *string* :
  The name of the application, default is the executable file name

 * `-os` *string* :
  The operating system to target (like GOOS) (default "linux")
